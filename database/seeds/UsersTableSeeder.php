<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Carlos Alberto Perea",
            'email' => "pachopunto@gmail.com",
            'password' => Hash::make('16221198'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Favian Castañeda",
            'email' => "asiselbordado@gmail.com",
            'password' => Hash::make('10027394'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Carlos Alberto Perea",
            'email' => "carlosperea05@hotmail.com",
            'password' => Hash::make('16221198'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Hilda Inés Córdoba",
            'email' => "neurocrece@gmail.com",
            'password' => Hash::make('26386423'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Juan Carlos Arias V",
            'email' => "frixionistas@gmail.com",
            'password' => Hash::make('14568184'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Andres Ronderos",
            'email' => "andresron78@gmail.com",
            'password' => Hash::make('16234441'),
            'rol' => "jefe"
        ]);
        DB::table('users')->insert([
            'name' => "Sebastian Cano",
            'email' => "sebascano65@gmail.com",
            'password' => Hash::make('1112788893'),
            'rol' => "desarrollador"
        ]);

    }
}
