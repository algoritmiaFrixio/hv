<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('boss_immediate', 50);
            $table->string('spot', 50);
            $table->unsignedBigInteger('id_enterprise');
            $table->foreign('id_enterprise')
                ->references('id')
                ->on('enterprises')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_gender');
            $table->foreign('id_gender')
                ->references('id')
                ->on('genders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('cargo_name', 100);
            $table->string('age_range', 20);
            $table->string('placement', 50);
            $table->boolean('commission')->nullable();
            $table->string('salary_assignment', 50)->nullable();
            $table->text('requested_profile');
            $table->text('features');
            $table->text('requirements');
            $table->text('basic_knowledge');
            $table->string('sheet_reception', 50);
            $table->unsignedBigInteger('chief_phone');
            $table->unsignedBigInteger('investment_networks');
            $table->string('time_diffusion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
