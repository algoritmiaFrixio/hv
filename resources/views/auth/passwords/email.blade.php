@extends('layouts.app')

@section('content')
    <div class="column"></div>
    <div class="column"></div>
    <section class="columns">
        <div class="column is-12-mobile is-6-desktop is-offset-3-desktop">
            <div class="card">
                <div class="card-header title box">{{__('Reset Password')}}</div>
                <div class="card-content">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="field">
                            <label for="email"
                                   class="label">{{ __('E-Mail Address') }}</label>
                            <div class="control">
                                <input id="email" type="email"
                                       class="input {{ $errors->has('password') ? ' is-danger' : '' }}"
                                       name="email" required>
                                @if ($errors->has('email'))
                                    <span class="help is-danger" role="alert">
                                        <strong>{{$errors->first('email')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="">
                            <button type="submit" class="button is-link">
                                {{__('Send Password Reset Link')}}
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
