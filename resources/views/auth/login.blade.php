@extends('layouts.app')

@section('content')
    <div class="column"></div>
    <div class="column"></div>
    <section class="columns">
        <div class="column is-12-mobile is-6-desktop is-offset-3-desktop">
            <div class="card">
                <div class="card-header title box">{{ __('Login') }}</div>
                <div class="card-content">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="field">
                            <label for="email" class="label">{{ __('E-Mail Address') }}</label>
                            <div class="control">
                                <input id="email" type="email"
                                       class="input {{ $errors->has('email') ? ' is-danger' : '' }}"
                                       name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span role="alert">
                                            <strong class="help is-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <label for="password"
                                   class="label">{{ __('Password') }}</label>
                            <div class="control">
                                <input id="password" type="password"
                                       class="input {{ $errors->has('password') ? ' is-danger' : '' }}"
                                       name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help is-danger" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <label class="checkbox" for="remember">
                                    <input class="" type="checkbox" name="remember"
                                           id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                        <div class="">
                            <button type="submit" class="button is-link">
                                {{ __('Login') }}
                            </button>

                            <a class="button is-text" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
