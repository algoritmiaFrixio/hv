@extends('layouts.app')
@section('content')
    @if (isset($messages))
        <div class="notification is-success">
            {{$messages}}
        </div>
    @endif
    @if (isset($messager))
        <div class="notification is-danger" id="message">
            {{$messager}}
        </div>
    @endif
    @yield('nav')
    <div class="container is-fluid " id="app">
        <strong
            style="font-size: 34px; color: red;">{{strtoupper('Protocolo para publicacion de convocatoria')}}</strong>
        <br>
        @php
            setlocale(LC_ALL,"es_ES");
        $fecha=strftime("%A, %d de %B de %Y");
        @endphp
        <strong>Fecha: {{$fecha}}</strong>
        <div class="column"></div>
        <form action="{{route('application.store')}}" method="POST">
            @csrf
            <div class="column"></div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="boss_immediate" class="label">Jefe inmediato con quien va a trabajar</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('boss_immediate') ? ' is-danger' : '' }}"
                                   id="boss_immediate" name="boss_immediate" value="{{ old('boss_immediate') }}"
                                   required autofocus>
                            @if ($errors->has('boss_immediate'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('boss_immediate') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="spot" class="label">Lugar donde va a ejercer sus funciones</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('spot') ? ' is-danger' : '' }}"
                                   id="spot" name="spot" value="{{ old('spot') }}" required>
                            @if ($errors->has('spot'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('spot') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="id_enterprise" class="label">Nombre de la empresa con la que se va a
                            vincular</label>
                        <div class="control">
                            <select type="text" class="input {{ $errors->has('id_enterprise') ? ' is-danger' : '' }}"
                                    id="id_enterprise" name="id_enterprise" required>
                                <option disabled value="" selected>Selecione un nombre de empresa</option>
                                @foreach($enterprises as $enterprise)
                                    <option value="{{$enterprise->id}}">{{$enterprise->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('id_enterprise'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('id_enterprise') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="cargo_name" class="label">Denominación del cargo</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('cargo_name') ? ' is-danger' : '' }}"
                                   id="cargo_name" name="cargo_name" value="{{ old('cargo_name') }}" required>
                            @if ($errors->has('cargo_name'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('cargo_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="age_range" class="label">Rango de Edad de la vacante</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('age_range') ? ' is-danger' : '' }}"
                                   id="age_range" name="age_range" value="{{ old('age_range') }}" required>
                            @if ($errors->has('age_range'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('age_range') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="placement" class="label">Ubicación Geográfica (la convocatoria es para que
                            ciudad)</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('placement') ? ' is-danger' : '' }}"
                                   id="placement" name="placement" value="{{ old('placement') }}" required>
                            @if ($errors->has('placement'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('placement') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="salary_assignment" class="label">Asignación básica salarial</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('salary_assignment') ? ' is-danger' : '' }}"
                                   id="salary_assignment" name="salary_assignment"
                                   value="{{ old('salary_assignment') }}" required>
                            @if ($errors->has('salary_assignment'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('salary_assignment') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <div class="control">
                            <h3>Sexo</h3>
                            @foreach($genders as $gender)
                                <label class="radio">
                                    <input type="radio" value="{{$gender->id}}" id="{{$gender->name}}"
                                           name="id_gender">
                                    {{$gender->name}}
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="field">
                        <label class="checkbox" for="commission">
                            <input type="checkbox" id="commission"
                                   name="commission">
                            Comisiones
                        </label>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="field">
                        <label for="requested_profile" class="label">Perfil del Cargo Solicitado</label>
                        <div class="control">
                            <textarea
                                class="textarea {{ $errors->has('requested_profile') ? ' is-danger' : '' }}"
                                id="requested_profile" name="requested_profile"
                                required>{{ old('requested_profile') }}</textarea>
                            @if ($errors->has('requested_profile'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('requested_profile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="field">
                        <label for="features" class="label">Funciones a realizar</label>
                        <div class="control">
                            <textarea
                                class="textarea {{ $errors->has('features') ? ' is-danger' : '' }}"
                                id="features" name="features"
                                required>{{ old('features') }}</textarea>
                            @if ($errors->has('features'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('features') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="field">
                        <label for="requirements" class="label">Requisitos</label>
                        <div class="control">
                            <textarea
                                class="textarea {{ $errors->has('requirements') ? ' is-danger' : '' }}"
                                id="requirements" name="requirements"
                                required>{{ old('requirements') }}</textarea>
                            @if ($errors->has('requirements'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('requirements') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="field">
                        <label for="basic_knowledge" class="label">Conocimientos básicos en:</label>
                        <div class="control">
                            <textarea
                                class="textarea {{ $errors->has('basic_knowledge') ? ' is-danger' : '' }}"
                                id="basic_knowledge" name="basic_knowledge"
                                required>{{ old('basic_knowledge') }}</textarea>
                            @if ($errors->has('basic_knowledge'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('basic_knowledge') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="sheet_reception" class="label">Correo Electrónico Recepción Hoja de Vida</label>
                        <div class="control">
                            <input type="email" class="input {{ $errors->has('sheet_reception') ? ' is-danger' : '' }}"
                                   id="sheet_reception" name="sheet_reception"
                                   value="{{ old('sheet_reception') }}" required>
                            @if ($errors->has('sheet_reception'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('sheet_reception') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="chief_phone" class="label">Teléfono de Contacto Jefe inmediato</label>
                        <div class="control">
                            <input type="tel" class="input {{ $errors->has('chief_phone') ? ' is-danger' : '' }}"
                                   id="chief_phone" name="chief_phone"
                                   value="{{ old('chief_phone') }}" required>
                            @if ($errors->has('chief_phone'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('chief_phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <div class="field">
                        <label for="investment_networks" class="label">Inversión en Redes Sociales</label>
                        <div class="control">
                            <input type="number"
                                   class="input {{ $errors->has('investment_networks') ? ' is-danger' : '' }}"
                                   id="investment_networks" name="investment_networks"
                                   value="{{ old('investment_networks') }}" required>
                            @if ($errors->has('investment_networks'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('investment_networks') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="time_diffusion" class="label">Tiempo de Difución</label>
                        <div class="control">
                            <input type="text" class="input {{ $errors->has('time_diffusion') ? ' is-danger' : '' }}"
                                   id="time_diffusion" name="time_diffusion"
                                   value="{{ old('time_diffusion') }}" required>
                            @if ($errors->has('time_diffusion'))
                                <span role="alert">
                                    <strong class="help is-danger">{{ $errors->first('time_diffusion') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns">
            </div>
            <div class="columns">
            </div>
            <div class="">
                <button class="button is-link" type="submit"> Crear solicitud</button>
            </div>
        </form>
    </div>
@endsection
