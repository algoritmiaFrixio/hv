<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    {{--<navbar-component--}}
    <nav class="navbar is-link">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">{{config('app.name','Leyes Office') }}</a>
            <div class="navbar-burger" data-target="navMenu" data-target="navbarMenuItems">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-end">
                @guest
                    <div class="navbar-item">
                        <div class="buttons">
                            <a href="{{ route('login') }}" class="button is-link">Entrar</a>
                        </div>
                    </div>

                @else
                    {{--                    <a href="{{route('query.directory')}}" class="navbar-item">Directorio</a>--}}
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">{{ Auth::user()->name }}</a>
                        <div class="navbar-dropdown is-boxed">
                            <a class="navbar-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Cerrar Sesión
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </a>
                            @endguest
                        </div>
                    </div>
            </div>
        </div>
    </nav>

    <main class="column">
        @yield('content')
    </main>
</div>
</body>
</html>
