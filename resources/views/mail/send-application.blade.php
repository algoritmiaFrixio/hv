@extends('layouts.app')
@section('content')
    @php
        setlocale(LC_ALL,"es_ES");
    @endphp
    <strong>Fecha: {{$application->created_at}}</strong>
    <form>
        @csrf
        <div class="column"></div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="boss_immediate" class="label">Jefe inmediato con quien va a trabajar</label>
                    <div class="control">
                        <input type="text" class="input"
                               id="boss_immediate" name="boss_immediate" value="{{$application->boss_immediate}}"
                               required autofocus>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label for="spot" class="label">Lugar donde va a ejercer sus funciones</label>
                    <div class="control">
                        <input type="text" class="input "
                               id="spot" name="spot" value="{{$application->spot}}" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="id_enterprise" class="label">Nombre de la empresa con la que se va a
                        vincular</label>
                    <div class="control">
                        <select type="text" class="input"
                                id="id_enterprise" name="id_enterprise" required>
                            <option disabled value="" selected>{{$application->enterprise->name}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label for="cargo_name" class="label">Denominación del cargo</label>
                    <div class="control">
                        <input type="text" class="input"
                               id="cargo_name" name="cargo_name" value="{{$application->cargo_name}}" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="age_range" class="label">Rango de Edad de la vacante</label>
                    <div class="control">
                        <input type="text" class="input "
                               id="age_range" name="age_range" value="{{$application->age_range}}" required>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label for="placement" class="label">Ubicación Geográfica (la convocatoria es para que
                        ciudad)</label>
                    <div class="control">
                        <input type="text" class="input"
                               id="placement" name="placement" value="{{$application->placement}}" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="salary_assignment" class="label">Asignación básica salarial</label>
                    <div class="control">
                        <input type="text" class="input"
                               id="salary_assignment" name="{{$application->salary_assignment}}"
                               value="{{$application->salary_assignment}}" required>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <div class="control">
                        <h3>Sexo</h3>
                        <label class="radio">
                            <input type="radio" value="gender" id="$gender->name"
                                   name="id_gender" checked>
                            {{$application->gender->name}}</label>
                    </div>
                </div>
                <div class="field">
                    <label class="checkbox" for="commission">
                        @if (is_null($application->commission))
                            <input type="checkbox" id="commission"
                                   name="commission">
                            Comisiones
                        @else
                            <input type="checkbox" checked id="commission"
                                   name="commission">
                            Comisiones
                        @endif
                    </label>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-12">
                <div class="field">
                    <label for="requested_profile" class="label">Perfil del Cargo Solicitado</label>
                    <div class="control">
                        <p>{{$application->requested_profile}}</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="columns">
            <div class="column is-12">
                <div class="field">
                    <label for="features" class="label">Funciones a realizar</label>
                    <div class="control">
                        <p>{{$application->features}}</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="columns">
            <div class="column is-12">
                <div class="field">
                    <label for="requirements" class="label">Requisitos</label>
                    <div class="control">
                        <p>{{$application->requirements}}</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="columns">
            <div class="column is-12">
                <div class="field">
                    <label for="basic_knowledge" class="label">Conocimientos básicos en:</label>
                    <div class="control">
                        <p>{{$application->basic_knowledge}}</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="sheet_reception" class="label">Correo Electrónico Recepción Hoja de Vida</label>
                    <div class="control">
                        {{$application->sheet_reception}}
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label for="chief_phone" class="label">Teléfono de Contacto Jefe inmediato</label>
                    <div class="control">
                        {{$application->chief_phone}}
                    </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column is-6">
                <div class="field">
                    <label for="investment_networks" class="label">Inversión en Redes Sociales</label>
                    <div class="control">
                        <input type="text"
                               class="input"
                               id="investment_networks" name="investment_networks"
                               value="{{$application->investment_networks}}" required>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label for="time_diffusion" class="label">Tiempo de Difución</label>
                    <div class="control">
                        <input type="text" class="input"
                               id="time_diffusion" name="time_diffusion"
                               value="{{$application->time_diffusion}}" required>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
