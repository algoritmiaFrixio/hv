<?php

namespace App\Mail;

use App\Applications;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplicationReceived extends Mailable
{
    use Queueable, SerializesModels;
    public $distressCall;

    /**
     * Create a new message instance.
     *
     * @param Applications $distressCall
     */
    public function __construct(Applications $distressCall)
    {
        $this->distressCall = $distressCall;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.send-application');
    }
}
