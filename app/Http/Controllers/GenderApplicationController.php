<?php

namespace App\Http\Controllers;

use App\GenderApplication;
use Illuminate\Http\Request;

class GenderApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GenderApplication  $genderApplication
     * @return \Illuminate\Http\Response
     */
    public function show(GenderApplication $genderApplication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GenderApplication  $genderApplication
     * @return \Illuminate\Http\Response
     */
    public function edit(GenderApplication $genderApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GenderApplication  $genderApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenderApplication $genderApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GenderApplication  $genderApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenderApplication $genderApplication)
    {
        //
    }
}
