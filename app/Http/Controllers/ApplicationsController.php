<?php

namespace App\Http\Controllers;

use App\Applications;
use App\Enterprise;
use App\Gender;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enterprises = Enterprise::all();
        $genders = Gender::all();
        return view('application.create-application', compact('enterprises', 'genders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "boss_immediate" => "required|string",
            "spot" => "required|string",
            "id_enterprise" => "required|integer",
            "id_gender" => "required|integer",
            "cargo_name" => "required|string",
            "age_range" => "required|string",
            "placement" => "required|string",
            "requested_profile" => "required|string",
            "features" => "required|string",
            "requirements" => "required|string",
            "basic_knowledge" => "required|string",
            "sheet_reception" => "required|string",
            "chief_phone" => "required",
            "investment_networks" => "required|integer",
            "time_diffusion" => "required|string",
        ]);
        try {
            $application = new Applications();
            $application->fill($request->all());
            $application->id_user = Auth::user()->id;
            $application->saveOrFail();
            $application->gender;
            $application->enterprise;
            $enterprises = Enterprise::all();
            $genders = Gender::all();
            $messages = "Agregado correctamente";
            $pdf = PDF::loadView('mail.send-application', compact('application'));
            $pdf->save('../files/' . $application->id . '.pdf');
            if ($this->mailer('Nueva solicitud de vacante', 'frixionistas@gmail.com', '../files/' . $application->id . '.pdf')) {
                if ($this->mailer('Nueva solicitud de vacante', 'neurocrece@gmail.com', '../files/' . $application->id . '.pdf')){
		    if ($this->mailer('Nueva solicitud de vacante', 'gepcol@hotmail.com','../files/' . $application->id . '.pdf'))
                    return view('application.create-application', compact('enterprises', 'genders', 'messages'));
		}
            }
            $messager = "Ocurrio un error vuelve a intentarlo";
            return view('application.create-application', compact('enterprises', 'genders', 'messager'));
        } catch (ModelNotFoundException $exception) {
            $enterprises = Enterprise::all();
            $genders = Gender::all();
            $messager = "Ocurrio un error vuelve a intentarlo";
            return view('application.create-application', compact('enterprises', 'genders', 'messager'));
        }
    }

    private function mailer(string $subject, string $email, string $path)
    {
        $transport = (new Swift_SmtpTransport('neurocrece.co', 465, 'ssl'))
            ->setUsername('info@neurocrece.co')
            ->setPassword('*ppc5cqpPf;[');
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message())
            ->setSubject($subject)
            ->setFrom(['info@neurocrece.co' => 'Solocitud Hoja de vida'])
            ->setTo($email)
            ->attach(\Swift_Attachment::fromPath($path));
        return $mailer->send($message);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function show(Applications $applications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function edit(Applications $applications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applications $applications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applications $applications)
    {
        //
    }
}
