<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Gender;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function hola()
    {
        $application = \App\Applications::where('id',2)->first();
        $application->gender;
        $application->enterprise;
        return view('mail.send-application', compact('application'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $enterprises = Enterprise::all();
        $genders = Gender::all();
        return view('home', compact('enterprises', 'genders'));
    }
}
